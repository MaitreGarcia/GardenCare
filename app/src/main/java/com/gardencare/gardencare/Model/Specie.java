package com.gardencare.gardencare.Model;

public class Specie {
    private int idSpecie;
    private String name;
    private int growTime;
    private String wetHour;
    private String wetAdvice;
    private String image;

    public Specie(String name) {
        this.name = name;
    }

    public int getIdSpecie() {
        return idSpecie;
    }

    public void setIdSpecie(int idSpecie) {
        this.idSpecie = idSpecie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrowTime() {
        return growTime;
    }

    public void setGrowTime(int growTime) {
        this.growTime = growTime;
    }

    public String getWetHour() {
        return wetHour;
    }

    public void setWetHour(String wetHour) {
        this.wetHour = wetHour;
    }

    public String getWetAdvice() {
        return wetAdvice;
    }

    public void setWetAdvice(String wetAdvice) {
        this.wetAdvice = wetAdvice;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public Specie(String name, int growTime, String wetHour, String wetAdvice)
    {
        this.name = name;
        this.growTime = growTime;
        this.wetHour = wetHour;
        this.wetAdvice = wetAdvice;
    }
    public void save(){}

    public String toString()
    {
        String str = "";
        str += this.name + ",\n";
        str += "WetHour : " + this.wetHour + "\n";
        str += "Mature in " + this.growTime + " days";
        return str;
    }
}