package com.gardencare.gardencare.Model;

public class NotificationManager{

    private Person person;

    public NotificationManager(Person person) {
        this.person = person;
    }

    /*
     * Checks wether the plants the person have to be weted or not and returns a string with the needed info
     */
    public String checkWeather() throws Exception {

        String result = "";

        for(Garden garden: person.getGardens()) {
            for (Plant plant : garden.getPlants()) {

                if(!plant.isMature(0)) {

                    Sky sky = new Sky();
                    String wetHour = plant.getWetHour();
                    String city = garden.getCity();

                    float temperature = Float.parseFloat(sky.getInfo(city, wetHour, "temp_c"));
                    float precipitation = Float.parseFloat(sky.getInfo(city, wetHour, "precip_mm"));
                    String tooHot = "";

                    if (temperature > 20)
                        tooHot = " beaucoup ";

                    if (precipitation < 0.4)
                        result += "Votre " + plant.getName() + " du jardin : " + garden.getName() + " doit être" + tooHot + " arrosé à " + wetHour + " H\n";
                }

                else
                    result += "Votre " + plant.getName() + " du jardin : " + garden.getName() + " doit être cueillie\n";
            }
        }


        return result;
    }
}
