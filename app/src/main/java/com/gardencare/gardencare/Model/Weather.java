package com.gardencare.gardencare.Model;

public class Weather {
    private static Sky sky;

    public static String getSky(String city,String hour) throws Exception {
        if(sky != null)
        {
            return sky.getWeather(city,hour);
        }
        sky = new Sky();
        return sky.getWeather(city,hour);
    }
}
