package com.gardencare.gardencare.Model;


public interface Observer {
    public void update(Object object);
}
