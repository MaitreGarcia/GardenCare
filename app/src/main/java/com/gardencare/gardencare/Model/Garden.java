package com.gardencare.gardencare.Model;

import com.gardencare.gardencare.Builder.GardenBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class Garden {
    private int idGarden;
    private ArrayList<Plant> plants;
    private String name;
    private String city;

    public Garden() {
        this.plants = new ArrayList<>();
    }

    public Garden (String name, String city)
    {
        this.plants = new ArrayList<>();
        this.name = name;
        this.city = city;
    }

    public int getIdGarden() {
        return idGarden;
    }

    public void setIdGarden(int idGarden) {
        this.idGarden = idGarden;
    }

    public ArrayList<Plant> getPlants() {
        return plants;
    }

    public void setPlants(ArrayList<Plant> plants) {
        this.plants = plants;
    }

    public void addPlant(Plant plant) {
        this.plants.add(plant);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void save(int idPerson) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://149.202.175.71/GardenCareService/public/garden/add");
        httpPost.addHeader("Content-type","application/x-www-form-urlencoded");
        httpPost.addHeader("Charset","UTF-8");

        BasicNameValuePair Vperson = new BasicNameValuePair("person","" + idPerson);
        BasicNameValuePair Vname = new BasicNameValuePair("name",this.name);
        BasicNameValuePair Vcity = new BasicNameValuePair("city",this.city);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(Vperson);
        params.add(Vname);
        params.add(Vcity);
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(params);
        httpPost.setEntity(urlEncodedFormEntity);


        HttpResponse httpResponse = httpClient.execute(httpPost);
        InputStream inputStream = httpResponse.getEntity().getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String bufferedStrChunk = null;

        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
            stringBuilder.append(bufferedStrChunk);
        }

        JSONObject o = new JSONObject(stringBuilder.toString());

        Garden garden = (Garden) new GardenBuilder().build(Integer.parseInt(o.get("idGarden").toString()));
        this.setIdGarden(garden.getIdGarden());
    }

    public String toString()
    {
        String str = "";
        str += "Name : " + this.name + "\n";
        str += "City : " + this.city + "\n";

        return str;
     }
}