package com.gardencare.gardencare.Builder;
import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.Model.Specie;
import com.loopj.android.http.HttpGet;

import org.json.JSONObject;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class PlantBuilder extends Builder {
    public Object build(int id) throws Exception {
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/plant/" + id;
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONObject o = new JSONObject(result);
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd");

        Plant plant = new Plant();
        plant.setIdPlant(Integer.parseInt(o.get("idPlant").toString()));
        plant.setMature(o.get("mature").toString());
        plant.setSpecie((Specie) new SpecieBuilder().build(Integer.parseInt(o.get("specie").toString())));
        plant.setPlantedAt(df.parse(o.get("plantedAt").toString()));

        return plant;
    }

    @Override
    public Object build(String email) throws Exception {
        return null;
    }
}

