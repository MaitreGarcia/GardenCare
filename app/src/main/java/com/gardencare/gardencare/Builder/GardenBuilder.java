package com.gardencare.gardencare.Builder;

import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Plant;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class GardenBuilder extends Builder
{
    private int id;

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return this.id;
    }


    public Object build(int id) throws Exception {
        this.setId(id);
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/garden/" + this.getId();
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONObject o = new JSONObject(result);

        Garden garden = new Garden();
        garden.setPlants(this.getPlants());
        garden.setName(o.get("name").toString());
        garden.setCity(o.get("city").toString());
        garden.setIdGarden(Integer.parseInt(o.get("idGarden").toString()));
        return garden;
    }

    @Override
    public Object build(String email) throws Exception {
        return null;
    }

    private ArrayList<Plant> getPlants() throws Exception {
        ArrayList<Plant> plants = new ArrayList<>();

        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/garden/" + this.getId() + "/plants";
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        Gson gson = new Gson();

        JSONArray jsonArray = new JSONArray(result);
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++)
        {
            list.add(jsonArray.get(i).toString());
        }

        for(int j = 0 ; j < list.size() ; j++)
        {
            JSONObject o = new JSONObject(list.get(j));
            plants.add((Plant)new PlantBuilder().build(Integer.parseInt(o.get("idPlant").toString())));
        }

        return plants;
    }
}
