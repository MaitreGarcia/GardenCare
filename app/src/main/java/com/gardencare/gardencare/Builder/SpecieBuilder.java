package com.gardencare.gardencare.Builder;

import com.gardencare.gardencare.Model.Specie;
import com.loopj.android.http.HttpGet;

import org.json.JSONObject;

import java.net.URI;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class SpecieBuilder extends Builder
{
    public Object build(int id) throws Exception {
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/specie/" + id;
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONObject o = new JSONObject(result);

        String idSpecie = o.get("idSpecie").toString();
        String name = o.get("name").toString();
        String growth_time = o.get("growth_time").toString();
        String wet_advice = o.get("wet_advice").toString();
        String wet_hour = o.get("wet_hour").toString();
        String image = o.get("image").toString();

        Specie specie = new Specie(name);
        specie.setIdSpecie(Integer.parseInt(idSpecie));
        specie.setGrowTime(Integer.parseInt(growth_time));
        specie.setWetAdvice(wet_advice);
        specie.setWetHour(wet_hour);
        specie.setImage(image);

        return specie;
    }

    @Override
    public Object build(String email) throws Exception {
        return null;
    }
}
