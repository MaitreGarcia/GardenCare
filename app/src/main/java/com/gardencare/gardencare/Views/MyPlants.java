package com.gardencare.gardencare.Views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gardencare.gardencare.Builder.GardenBuilder;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.R;

public class MyPlants extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plants);

        FloatingActionButton addPlant = (FloatingActionButton) findViewById(R.id.addPlant);

        addPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyPlants.this,AddPlant.class);
                intent.putExtra("idGarden",getIntent().getIntExtra("idGarden",-1));
                startActivity(intent);
            }
        });
        new PlantsGarden().execute();
    }

    private class PlantsGarden extends AsyncTask<String, Void, Garden>{
        @Override
        protected Garden doInBackground(String... strings) {
            Intent intent = getIntent();
            try {
                return (Garden) new GardenBuilder().build(intent.getIntExtra("idGarden",0));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Garden garden) {
            try {
                for(final Plant plant : garden.getPlants()){

                    ContextThemeWrapper newContext = new ContextThemeWrapper(MyPlants.this, R.style.Button);
                    Button b = new Button(newContext);
                    b.setText(plant.toString());

                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyPlants.this,PlantActivity.class);
                            intent.putExtra("idPlant",plant.getIdPlant());
                            startActivity(intent);
                        }
                    });

                    LinearLayout layout = (LinearLayout) findViewById(R.id.linear_layout_plants);
                    layout.addView(b);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
