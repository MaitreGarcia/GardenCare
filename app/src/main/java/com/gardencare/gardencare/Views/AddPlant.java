package com.gardencare.gardencare.Views;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SpinnerAdapter;

import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.Model.Specie;
import com.gardencare.gardencare.R;
import com.gardencare.gardencare.WebServices.Species;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


public class AddPlant extends AppCompatActivity {
    private int idGarden = -1;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_plant);

        this.idGarden = getIntent().getIntExtra("idGarden",-1);

        new SpeciesService().execute();

        EditText plantedAt = (EditText) findViewById(R.id.plantedAt);
        Button submit = (Button) findViewById(R.id.submitPlant);


        plantedAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(AddPlant.this,
                        new mDateSetListener(), mYear, mMonth, mDay);
                dialog.show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetSpecieService().execute(((EditText) findViewById(R.id.species)).getText().toString());
            }
        });


    }

    private class AddPlantService extends AsyncTask<Plant, Void, Void> {

        @Override
        protected Void doInBackground(Plant... plants) {
            try {

                plants[0].save(AddPlant.this.idGarden);
                Intent intent = new Intent(AddPlant.this, MyPlants.class);
                intent.putExtra("idGarden",AddPlant.this.idGarden);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class GetSpecieService extends AsyncTask<String, Void, Specie> {

        @Override
        protected Specie doInBackground(String... strings) {
            try {
                return Species.getSpecie(strings[0]);
            } catch (Exception e) {
                return null;
            }

        }


        @Override
        protected void onPostExecute(Specie specie) {
            final EditText plantedAt = (EditText) findViewById(R.id.plantedAt);


            Plant p = new Plant();
            String[] members = plantedAt.getText().toString().split("/");

            Calendar cal = new GregorianCalendar(Integer.parseInt(members[0].trim()),Integer.parseInt(members[1].trim()), Integer.parseInt(members[2].trim()));
            System.out.println(Arrays.toString(members));
            System.out.println(cal.getTime());
            p.setPlantedAt(cal.getTime());
            p.setMature("no");
            p.setSpecie(specie);
            new AddPlantService().execute(p);
        }
    }

    private class mDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            EditText plantedAt = (EditText) findViewById(R.id.plantedAt);

            plantedAt.setText(new StringBuilder()
                    // Month is 0 based so add 1

                    .append(year).append("/")
                    .append(monthOfYear + 1).append("/").append(dayOfMonth));
        }

    }

    private class SpeciesService extends AsyncTask<String[], SpinnerAdapter, String[]> {
        @Override
        protected String[] doInBackground(String[]... species) {
            try {
                List<String> speciesName = new ArrayList<>();
                ArrayList<Specie> speciesList = Species.getSpecies();
                for (int i = 0; i < speciesList.size(); i++) {
                    speciesName.add(speciesList.get(i).getName());
                }
                String[] arr = new String[speciesName.size()];
                arr = speciesName.toArray(arr);
                return arr;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] strings) {
            AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.species);
            ArrayAdapter<String> adapter =
                    new ArrayAdapter<>(AddPlant.this, android.R.layout.simple_list_item_1, strings);
            autoCompleteTextView.setAdapter(adapter);
        }
    }
}