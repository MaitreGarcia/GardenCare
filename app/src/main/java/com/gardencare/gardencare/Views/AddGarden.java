package com.gardencare.gardencare.Views;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.gardencare.gardencare.Builder.PersonBuilder;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Person;
import com.gardencare.gardencare.R;

public class AddGarden extends AppCompatActivity{
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_garden);



        Button submit = (Button) findViewById(R.id.submitGarden);

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                EditText name = (EditText) findViewById(R.id.gardenName);
                EditText city = (EditText) findViewById(R.id.gardenCity);
                Garden g = new Garden();
                g.setCity(city.getText().toString());
                g.setName(name.getText().toString());

                new AddGardenService().execute(g);

            }
        });
    }

    private class AddGardenService extends AsyncTask<Garden, Void, Void>{

        @Override
        protected Void doInBackground(Garden... gardens) {
            try {
                Person p = (Person)new PersonBuilder().build(Login.getLogin(getApplicationContext().getSharedPreferences("com.example.app", Context.MODE_PRIVATE)));
                gardens[0].save(p.getIdPerson());
                startActivity(new Intent(AddGarden.this,MyGardens.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
