package com.gardencare.gardencare;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gardencare.gardencare.Builder.SpecieBuilder;
import com.gardencare.gardencare.Model.Specie;
import com.gardencare.gardencare.WebServices.Species;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TestSpeciesImage {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Specie s = (Specie) new SpecieBuilder().build(5);
        Specie s2 = Species.getSpecie("Onion");

        assertEquals(s.getIdSpecie(),s2.getIdSpecie());
    }
}
