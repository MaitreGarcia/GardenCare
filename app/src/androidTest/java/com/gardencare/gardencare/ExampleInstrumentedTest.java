package com.gardencare.gardencare;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import java.util.Date;

import com.gardencare.gardencare.Model.Person;
import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.Model.Sky;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Specie;
import com.gardencare.gardencare.Model.NotificationManager;
import com.gardencare.gardencare.WebServices.Species;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Sky sky = new Sky();
        String temperature = sky.getInfo("Paris","15","temp_c");

        Person person = new Person();

        Garden garden1 = new Garden("jardin1","Paris");

        Garden garden2 = new Garden("jardin1","Paris");

        Specie specie = new Specie("tomate");
        specie.setGrowTime(120);
        specie.setWetHour("17");

        Specie specie2 = new Specie("patate");
        specie2.setGrowTime(120);
        specie2.setWetHour("17");

        Plant plant1 = new Plant();

        plant1.setMature("no");
        plant1.setSpecie(specie);
        plant1.setPlantedAt(new Date());

        Plant plant2 = new Plant();

        plant2.setMature("no");
        plant2.setSpecie(specie2);
        plant2.setPlantedAt(new Date());

        garden1.addPlant(plant1);
        garden2.addPlant(plant2);

        person.addGarden(garden1);
        person.addGarden(garden2);

        assertEquals(Species.getSpecies().size(),1);
    }
}